package utils.maths;

import java.util.Random;

/**
 * Provides Maths utilities methods
 * @author m.renault
 *
 */
public final class MathUtils
{
	//--Constants
	private static final Random RNG = new Random();

	//--Utils

	/** Indicates if the given number is even. */
	public static boolean isEven(int n)
	{
		return n % 2 == 0;
	}

	/** Indicates if the given number is odd. */
	public static boolean isOdd(int n)
	{
		return !isEven(n);
	}

	/**
	 * Clamps the given value between min and max
	 * @param value the value to clamp
	 * @param min the min value
	 * @param max the max value
	 * @return the clamped value
	 */
	public static int clamp(int value, int min, int max)
	{
		min = Math.min(min, max);
		max = Math.max(min, max);

		return value < min ? min : value > max ? max : value;
	}

	/**
	 * Clamps the given value between min and max
	 * @param value the value to clamp
	 * @param min the min value
	 * @param max the max value
	 * @return the clamped value
	 */
	public static float clamp(float value, float min, float max)
	{
		min = Math.min(min, max);
		max = Math.max(min, max);

		return value < min ? min : value > max ? max : value;
	}

	//--Random

	/**
	 * Gets a random number between min and max
	 * @param min the min value (inclusive)
	 * @param max the max value (exclusive)
	 * @return the random value
	 */
	public static int random(int min, int max)
	{
		return (int) (min + RNG.nextFloat() * (max - min));
	}

	/**
	 * Gets a random number between min and max
	 * @param min the min value (inclusive)
	 * @param max the max value (exclusive)
	 * @return the random value
	 */
	public static float random(float min, float max)
	{
		return min + RNG.nextFloat() * (max - min);
	}

	/**
	 * Obtains a random value from the given array
	 * @param values an array of value
	 * @return a random value from the array
	 */
	public static <V> V random(V[] values)
	{
		return values != null && values.length > 0 ? values[random(0, values.length-1)] : null;
	}

	//--Interpolation

	/** Linearly interpolates between min and max by progression
	 * @param min the min bound
	 * @param max the max bound
	 * @param progression the progression (must be between 0 and 1)
	 * @return
	 */
	public static float lerp(float min, float max, float progression)
	{
		return min + (max - min) * clamp(progression, 0F, 1F);
	}
}
