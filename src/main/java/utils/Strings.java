package utils;

import java.util.Collection;
import java.util.Iterator;

import com.google.common.base.Preconditions;

/**
 * Provides utilities methods to operate on String objects.
 * @author m.renault
 */
public final class Strings
{
	//--Constants

	/** The new line character. */
	public static final String NEWLINE = "\n";

	/** The pattern mark used to replace with values in {@link #format}. */
	public static final String PATTERN_MARK = "{}";

	//--Join

	/**
	 * Joins the given values by using a separator
	 * @param separator the string to insert between values
	 * @param values the values to join
	 * @return a string of joined values, each separated by the separator
	 */
	public static String join(String separator, Object ... values)
	{
		return join(separator, values, values.length);
	}

	/**
	 * Joins the given values by using a separator
	 * @param separator the string to insert between values
	 * @param values the values to join
	 * @return a string of joined values, each separated by the separator
	 */
	public static String join(String separator, Collection<?> values)
	{
		return join(separator, values, values.size());
	}

	/**
	 * Joins the given values by using a separator
	 * @param separator the string to insert between values
	 * @param values the values to join
	 * @return a string of joined values, each separated by the separator
	 */
	public static String join(String separator, Object[] values, int size)
	{
		StringBuilder builder = new StringBuilder();

		int limit = Math.min(values.length, size);

		for(int i = 0; i < limit; i++)
		{
			builder.append(String.valueOf(values[i]));

			if(i < limit-1)
			{
				builder.append(separator);
			}
		}

		return builder.toString();
	}

	/**
	 * Joins the given values by using a separator
	 * @param separator the string to insert between values
	 * @param values the values to join
	 * @return a string of joined values, each separated by the separator
	 */
	public static String join(String separator, Collection<?> values, int size)
	{
		StringBuilder builder = new StringBuilder();

		int limit = Math.min(values.size(), size);
		Iterator<?> iterator = values.iterator();

		for(int i = 0; i < limit && iterator.hasNext(); i++)
		{
			builder.append(String.valueOf(iterator.next()));

			if(i < limit-1)
			{
				builder.append(separator);
			}
		}

		return builder.toString();
	}

	//--Format

	/**
	 * Formats the given pattern using arguments by replacing each <b>{}</b> by the corresponding value in the argument array
	 * @param pattern the pattern to format
	 * @param args the arguments to use
	 * @return a formatted string
	 */
	public static String format(String pattern, Object ... args)
	{
		//Returns empty if pattern is null
		if(pattern == null || pattern.isEmpty())
		{
			return "";
		}

		//Return pattern if no arguments
		if(args == null || args.length == 0)
		{
			return pattern;
		}

		StringBuilder s = new StringBuilder();
		int argCounter = 0;

		int index = 0;
		while((index = pattern.indexOf(PATTERN_MARK)) != -1 && argCounter < args.length)
		{
			//Append valid
			s.append(pattern.substring(0, index));
			s.append(args[argCounter++]);

			//Substring pattern from argument mark index
			pattern = pattern.substring(index + PATTERN_MARK.length());
		}

		//Append remaining string
		s.append(pattern);

		return s.toString();
	}

	//--Misc

	/** Capitalizes the given string. */
	public static String capitalize(String string)
	{
		if(string == null || string.length() == 0)
		{
			return "";
		}

		return Character.toUpperCase(string.charAt(0)) + string.substring(1);
	}

	/**
	 * Repeat the given character n times.
	 * @param c the character to repeat
	 * @param n the number of time to repeat
	 * @return a String of n times repeating character
	 */
	public static String repeat(char c, int n)
	{
		if(n <= 0)
		{
			return "";
		}
		else if(n == 1)
		{
			return String.valueOf(c);
		}
		else
		{
			StringBuilder builder = new StringBuilder();

			for(int i = 0; i < n; i++)
			{
				builder.append(c);
			}

			return builder.toString();
		}
	}

	/**
	 * Repeat the given string n times.
	 * @param s the string to repeat
	 * @param n the number of time to repeat
	 * @return a String of n times repeating string
	 */
	public static String repeat(String s, int n)
	{
		Preconditions.checkState(n > 1, "Repeat time cannot be inferior to 0");

		if(n == 0)
		{
			return "";
		}
		else if(n == 1)
		{
			return s;
		}
		else
		{
			StringBuilder builder = new StringBuilder();

			for(int i = 0; i < n; i++)
			{
				builder.append(s);
			}

			return builder.toString();
		}
	}
}
