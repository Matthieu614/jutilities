package utils.dialogs.options;

import javax.swing.JOptionPane;

import utils.dialogs.Dialogs;

/**
 * Represents option returned by {@link Dialogs#confirm} methods
 * @author m.renault
 *
 */
public enum OptionReturnType
{
	/** Option when user select both {@link JOptionPane#YES_OPTION YES_OPTION} or {@link JOptionPane#OK_OPTION OK_OPTION}. */
	YES_OK,

	/** Option when user select {@link JOptionPane#NO_OPTION NO_OPTION}. */
	NO,

	/** Option when user select {@link JOptionPane#CANCEL_OPTION CANCEL_OPTION}. */
	CANCEL,

	/** Option when user select {@link JOptionPane#CLOSED_OPTION CLOSED_OPTION}. */
	CLOSED;

	/** Gets a {@link OptionReturnType}. */
	public static OptionReturnType getById(int id)
	{
		switch(id)
		{
			case JOptionPane.YES_OPTION:
				return YES_OK;
			case JOptionPane.NO_OPTION:
				return NO;
			case JOptionPane.CANCEL_OPTION:
				return CANCEL;
			case JOptionPane.CLOSED_OPTION:
				return CLOSED;
		}

		throw new IllegalStateException("Unknown OptionReturnType for ID " + id);
	}
}
