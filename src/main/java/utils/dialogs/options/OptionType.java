package utils.dialogs.options;

import javax.swing.JOptionPane;

/**
 * Represents option type that describes buttons.
 * @author m.renault
 */
public enum OptionType
{
	/** [YES|NO] Option buttons. */
	YES_NO(JOptionPane.YES_NO_OPTION),

	/** [YES|NO|CANCEL] Option buttons. */
	YES_NO_CANCEL(JOptionPane.YES_NO_CANCEL_OPTION),

	/** [OK|CANCEL] Option buttons. */
	OK_CANCEL(JOptionPane.OK_CANCEL_OPTION);

	//--Properties
	private final int swingValue;

	OptionType(int swingValue)
	{
		this.swingValue = swingValue;
	}

	/** Gets the swing value of this {@link OptionType}. */
	public int getSwingValue()
	{
		return this.swingValue;
	}
}
