package utils.dialogs;

import javax.swing.JOptionPane;

/**
 * Represent a message type
 * @author m.renault
 */
public enum MessageType
{
	/** An information message type. */
	PLAIN(JOptionPane.PLAIN_MESSAGE),

	/** An information message type. */
	INFORMATION(JOptionPane.INFORMATION_MESSAGE),

	/** An error message type. */
	ERROR(JOptionPane.ERROR_MESSAGE),

	/** A question message type. */
	QUESTION(JOptionPane.QUESTION_MESSAGE),

	/** A warning message type. */
	WARNING(JOptionPane.WARNING_MESSAGE);

	//--Properties
	private final int swingValue;

	MessageType(int swingValue)
	{
		this.swingValue = swingValue;
	}

	/** Gets the swing value of this {@link MessageType}. */
	public int getSwingValue()
	{
		return this.swingValue;
	}
}
