package utils.dialogs;

import javax.swing.JOptionPane;

import utils.dialogs.options.OptionReturnType;
import utils.dialogs.options.OptionType;

/**
 * Provides utilities methods to display input and output dialogs
 * @author m.renault
 */
public final class Dialogs
{
	//--Output

	/**
	 * Shows a message dialog with a message, no title and {@link MessageType#INFORMATION INFORMATION} type
	 * @param message the message to be shown
	 */
	public static void message(Object message)
	{
		message(message, (String)null);
	}

	/**
	 * Shows a message dialog with a message and title.
	 * @param message the message to be shown
	 * @param title the title of the dialog
	 */
	public static void message(Object message, String title)
	{
		message(message, title, null);
	}

	/**
	 * Shows a message dialog with a message and type. <br>If <code>Type</code> is null then it use {@link MessageType#PLAIN}
	 * @param message the message to be shown
	 * @param type the type of the message (one of {@link MessageType#PLAIN}, {@link MessageType#INFORMATION}, {@link MessageType#QUESTION}, {@link MessageType#ERROR} and {@link MessageType#WARNING})
	 */
	public static void message(Object message, MessageType type)
	{
		message(message, null, type);
	}

	/**
	 * Shows a message dialog with a message, title and type. <br>If <code>Type</code> is null then it use {@link MessageType#PLAIN}
	 * @param message the message to be shown
	 * @param title the title of the dialog
	 * @param type the type of the message (one of {@link MessageType#PLAIN}, {@link MessageType#INFORMATION}, {@link MessageType#QUESTION}, {@link MessageType#ERROR} and {@link MessageType#WARNING})
	 */
	public static void message(Object message, String title, MessageType type)
	{
		JOptionPane.showMessageDialog(null, message, title, type != null ? type.getSwingValue() : MessageType.PLAIN.getSwingValue());
	}

	//--Input

	/** Shows an input dialog with a message
	 * @param message the message to be shown
	 * @return the user input or null
	 */
	public static String input(Object message)
	{
		return input(message, (String)null);
	}


	/** Shows an input dialog with a message and type
	 * @param message the message to be shown
	 * @param type the type of the message (one of {@link MessageType#PLAIN}, {@link MessageType#INFORMATION}, {@link MessageType#QUESTION}, {@link MessageType#ERROR} and {@link MessageType#WARNING})
	 * @return the user input or null
	 */
	public static String input(Object message, MessageType type)
	{
		return input(message, null, type);
	}

	/** Shows an input dialog with a message and title
	 * @param message the message to be shown
	 * @param title the title of the dialog
	 * @return the user input or null
	 */
	public static String input(Object message, String title)
	{
		return input(message, title, null);
	}

	/** Shows an input dialog with a message, title and type
	 * @param message the message to be shown
	 * @param title the title of the dialog
	 * @param type the type of the message (one of {@link MessageType#PLAIN}, {@link MessageType#INFORMATION}, {@link MessageType#QUESTION}, {@link MessageType#ERROR} and {@link MessageType#WARNING})
	 * @return the user input or null
	 */
	public static String input(Object message, String title, MessageType type)
	{
		return JOptionPane.showInputDialog(null, message, title, type != null ? type.getSwingValue() : MessageType.PLAIN.getSwingValue());
	}

	//--InputSelection

	/**
	 * Shows an input selection dialog with a message and a list of possible values
	 * @param message the message to be displayed
	 * @param selectionValues an array of possible value
	 * @return the selected value or null
	 */
	public static <V> V inputSelection(Object message, V[] selectionValues)
	{
		return inputSelection(message, (String)null, selectionValues, null);
	}

	/**
	 * Shows an input selection dialog with a message, a list of possible values and an initial value
	 * @param message the message to be displayed
	 * @param selectionValues an array of possible value
	 * @param initialValue the default selected value
	 * @return the selected value or null
	 */
	public static <V> V inputSelection(Object message, V[] selectionValues, V initialValue)
	{
		return inputSelection(message, (String)null, selectionValues, initialValue);
	}

	/**
	 * Shows an input selection dialog with a message, title, and a list of possible values
	 * @param message the message to be displayed
	 * @param title the title of the dialog
	 * @param selectionValues an array of possible value
	 * @return the selected value or null
	 */
	public static <V> V inputSelection(Object message, String title, V[] selectionValues)
	{
		return inputSelection(message, title, null, selectionValues, null);
	}

	/**
	 * Shows an input selection dialog with a message, title, a list of possible values and an initial value
	 * @param message the message to be displayed
	 * @param title the title of the dialog
	 * @param selectionValues an array of possible value
	 * @param initialValue the default selected value
	 * @return the selected value or null
	 */
	public static <V> V inputSelection(Object message, String title, V[] selectionValues, V initialValue)
	{
		return inputSelection(message, title, null, selectionValues, initialValue);
	}

	/**
	 * Shows an input selection dialog with a message, type, and a list of possible values
	 * @param message the message to be displayed
	 * @param type the type of the message (one of {@link MessageType#PLAIN}, {@link MessageType#INFORMATION}, {@link MessageType#QUESTION}, {@link MessageType#ERROR} and {@link MessageType#WARNING})
	 * @param selectionValues an array of possible value
	 * @return the selected value or null
	 */
	public static <V> V inputSelection(Object message, MessageType type, V[] selectionValues)
	{
		return inputSelection(message, null, type, selectionValues, null);
	}

	/**
	 * Shows an input selection dialog with a message, type, a list of possible values and an initial value
	 * @param message the message to be displayed
	 * @param type the type of the message (one of {@link MessageType#PLAIN}, {@link MessageType#INFORMATION}, {@link MessageType#QUESTION}, {@link MessageType#ERROR} and {@link MessageType#WARNING})
	 * @param selectionValues an array of possible value
	 * @param initialValue the default selected value
	 * @return the selected value or null
	 */
	public static <V> V inputSelection(Object message, MessageType type, V[] selectionValues, V initialValue)
	{
		return inputSelection(message, null, type, selectionValues, initialValue);
	}

	/**
	 * Shows an input selection dialog with a message, title, type and a list of possible values
	 * @param message the message to be displayed
	 * @param title the title of the dialog
	 * @param type the type of the message (one of {@link MessageType#PLAIN}, {@link MessageType#INFORMATION}, {@link MessageType#QUESTION}, {@link MessageType#ERROR} and {@link MessageType#WARNING})
	 * @param selectionValues an array of possible value
	 * @return the selected value or null
	 */
	public static <V> V inputSelection(Object message, String title, MessageType type, V[] selectionValues)
	{
		return inputSelection(message, title, type, selectionValues, null);
	}

	/**
	 * Shows an input selection dialog with a message, title, type, a list of possible values and an initial value
	 * @param message the message to be displayed
	 * @param title the title of the dialog
	 * @param type the type of the message (one of {@link MessageType#PLAIN}, {@link MessageType#INFORMATION}, {@link MessageType#QUESTION}, {@link MessageType#ERROR} and {@link MessageType#WARNING})
	 * @param selectionValues an array of possible value
	 * @param initialValue the default selected value
	 * @return the selected value or null
	 */
	public static <V> V inputSelection(Object message, String title, MessageType type, V[] selectionValues, V initialValue)
	{
		return (V) JOptionPane.showInputDialog(null, message, title, type != null ? type.getSwingValue() : MessageType.PLAIN.getSwingValue(), null, selectionValues, initialValue);
	}

	//--Confirm

	/**
	 * Shows a confirmation dialog with a message, no title, {@link OptionType#OK_CANCEL OK_CANCEl} options and {@link MessageType#PLAIN PLAIN} message type
	 * @param message the message to be displayed
	 * @return the option the user choose (one of {@link OptionReturnType#YES_OK YES_OK}, {@link OptionReturnType#NO NO}, {@link OptionReturnType#CANCEL CANCEL} and {@link OptionReturnType#CANCEL CLOSED})
	 */
	public static OptionReturnType confirm(Object message)
	{
		return confirm(message, (String)null);
	}

	/**
	 * Shows a confirmation dialog with a message, no title, {@link OptionType#OK_CANCEL OK_CANCEl} options and message type
	 * @param message the message to be displayed
	 * @param messageType the type of the message (one of {@link MessageType#PLAIN PLAIN}, {@link MessageType#INFORMATION INFORMATION}, {@link MessageType#QUESTION QUESTION}, {@link MessageType#ERROR ERROR} and {@link MessageType#WARNING WARNING})
	 * @return the option the user choose (one of {@link OptionReturnType#YES_OK YES_OK}, {@link OptionReturnType#NO NO}, {@link OptionReturnType#CANCEL CANCEL} and {@link OptionReturnType#CANCEL CLOSED})
	 */
	public static OptionReturnType confirm(Object message, MessageType messageType)
	{
		return confirm(message, (String)null, null, messageType);
	}

	/**
	 * Shows a confirmation dialog with a message, no title, options and {@link MessageType#PLAIN PLAIN} message type
	 * @param message the message to be displayed
	 * @return the option the user choose (one of {@link OptionReturnType#YES_OK YES_OK}, {@link OptionReturnType#NO NO}, {@link OptionReturnType#CANCEL CANCEL} and {@link OptionReturnType#CANCEL CLOSED})
	 */
	public static OptionReturnType confirm(Object message, OptionType optionType)
	{
		return confirm(message, null, optionType);
	}

	/**
	 * Shows a confirmation dialog with a message, no title, options and message type
	 * @param message the message to be displayed
	 * @param optionType the type of option (one of {@link OptionType#YES_NO YES_NO}, {@link OptionType#YES_NO_CANCEL NO_CANCEL} and {@link OptionType#OK_CANCEL CANCEL})
	 * @param messageType the type of the message (one of {@link MessageType#PLAIN PLAIN}, {@link MessageType#INFORMATION INFORMATION}, {@link MessageType#QUESTION QUESTION}, {@link MessageType#ERROR ERROR} and {@link MessageType#WARNING WARNING})
	 * @return the option the user choose (one of {@link OptionReturnType#YES_OK YES_OK}, {@link OptionReturnType#NO NO}, {@link OptionReturnType#CANCEL CANCEL} and {@link OptionReturnType#CANCEL CLOSED})
	 */
	public static OptionReturnType confirm(Object message, OptionType optionType, MessageType messageType)
	{
		return confirm(message, null, optionType, messageType);
	}

	/**
	 * Shows a confirmation dialog with a message, title, {@link OptionType#OK_CANCEL OK_CANCEl} options and {@link MessageType#PLAIN PLAIN} message type
	 * @param message the message to be displayed
	 * @return the option the user choose (one of {@link OptionReturnType#YES_OK YES_OK}, {@link OptionReturnType#NO NO}, {@link OptionReturnType#CANCEL CANCEL} and {@link OptionReturnType#CANCEL CLOSED})
	 */
	public static OptionReturnType confirm(Object message, String title)
	{
		return confirm(message, title, null, null);
	}

	/**
	 * Shows a confirmation dialog with a message, title, {@link OptionType#OK_CANCEL OK_CANCEl} options and message type
	 * @param message the message to be displayed
	 * @return the option the user choose (one of {@link OptionReturnType#YES_OK YES_OK}, {@link OptionReturnType#NO NO}, {@link OptionReturnType#CANCEL CANCEL} and {@link OptionReturnType#CANCEL CLOSED})
	 */
	public static OptionReturnType confirm(Object message, String title, MessageType messageType)
	{
		return confirm(message, title, null, messageType);
	}

	/**
	 * Shows a confirmation dialog with a message, no title, options and {@link MessageType#PLAIN PLAIN} message type
	 * @param message the message to be displayed
	 * @return the option the user choose (one of {@link OptionReturnType#YES_OK YES_OK}, {@link OptionReturnType#NO NO}, {@link OptionReturnType#CANCEL CANCEL} and {@link OptionReturnType#CANCEL CLOSED})
	 */
	public static OptionReturnType confirm(Object message, String title, OptionType optionType)
	{
		return confirm(message, title, optionType, null);
	}

	/**
	 * Shows a confirmation dialog with a message, title, options and message type
	 * @param message the message to be displayed
	 * @param title the title of the dialog
	 * @param optionType the type of option (one of {@link OptionType#YES_NO YES_NO}, {@link OptionType#YES_NO_CANCEL NO_CANCEL} and {@link OptionType#OK_CANCEL CANCEL})
	 * @param messageType the type of the message (one of {@link MessageType#PLAIN PLAIN}, {@link MessageType#INFORMATION INFORMATION}, {@link MessageType#QUESTION QUESTION}, {@link MessageType#ERROR ERROR} and {@link MessageType#WARNING WARNING})
	 * @return the option the user choose (one of {@link OptionReturnType#YES_OK YES_OK}, {@link OptionReturnType#NO NO}, {@link OptionReturnType#CANCEL CANCEL} and {@link OptionReturnType#CANCEL CLOSED})
	 */
	public static OptionReturnType confirm(Object message, String title, OptionType optionType, MessageType messageType)
	{
		return OptionReturnType.getById(JOptionPane.showConfirmDialog(null, message, title, optionType != null ? optionType.getSwingValue() : OptionType.OK_CANCEL.getSwingValue(), messageType != null ? messageType.getSwingValue() : MessageType.PLAIN.getSwingValue()));
	}
}
