package utils.log;

import java.nio.charset.StandardCharsets;

import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.slf4j.impl.StaticLoggerBinder;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.encoder.Encoder;
import utils.Strings;

/**
 * A {@link Logger} static implementation for easy-of-use logging
 * @author m.renault
 */
public final class LogAgent
{
	//--Constant.
	private static final int NO_INDENT = 0;

	private static final String PATTERN_DEFAULT = "\\(%d{HH:mm:ss}\\) %logger{0} %-5level | %message%n";

	/** The {@link Logger}. */
	private static final Logger LOGGER;

	/** Makes an indented message. */
	private static String indent(int n, Object message)
	{
		return Strings.repeat(' ', n) + String.valueOf(message);
	}

	static
	{
		//Reset default factory
		((LoggerContext)StaticLoggerBinder.getSingleton().getLoggerFactory()).reset();

		LOGGER = (Logger)LoggerFactory.getLogger("STDOUT");
		LOGGER.detachAndStopAllAppenders();

		LoggerContext context = LOGGER.getLoggerContext();

		//Console
		ConsoleAppender<?> consoleAppender = new ConsoleAppender<>();
		consoleAppender.setContext(context);
		consoleAppender.setName("STDOUT-Console");

		PatternLayoutEncoder encoder = new PatternLayoutEncoder();
		encoder.setContext(context);
		encoder.setPattern(PATTERN_DEFAULT);
		encoder.setCharset(StandardCharsets.UTF_8);
		encoder.start();

		consoleAppender.setEncoder((Encoder)encoder);
		consoleAppender.start();

		LOGGER.addAppender((Appender)consoleAppender);
	}

	//--INFO

	/**
	 * Logs an {@link Level#INFO INFO} message.
	 * @param message the message to log
	 */
	public static void info(Object message)
	{
		info(NO_INDENT, message);
	}

	/**
	 * Logs an {@link Level#INFO INFO} message with pattern.
	 * @param pattern the message pattern
	 * @param args the arguments to replace
	 */
	public static void info(String pattern, Object ... args)
	{
		info(Strings.format(pattern, args));
	}

	/**
	 * Logs an {@link Level#INFO INFO} message of the specified {@link Level} with a {@link Throwable}.
	 * @param throwable the throwable to trace
	 * @param message the message to log
	 */
	public static void info(Throwable throwable, String message)
	{
		info(NO_INDENT, throwable, message);
	}

	/**
	 * Logs an {@link Level#INFO INFO} message with pattern and {@link Throwable}.
	 * @param throwable the throwable to trace
	 * @param pattern the message pattern
	 * @param args the arguments to replace with
	 */
	public static void info(Throwable throwable, String pattern, Object ... args)
	{
		info(throwable, Strings.format(pattern, args));
	}

	/**
	 * Logs an indented {@link Level#INFO INFO} message.
	 * @param indent the number of space indent
	 * @param message the message to log
	 */
	public static void info(int indent, Object message)
	{
		LOGGER.info(indent(indent, message));
	}

	/**
	 * Logs an indented {@link Level#INFO INFO} message with pattern.
	 * @param indent the number of space indent
	 * @param pattern the message pattern
	 * @param args the arguments to replace
	 */
	public static void info(int indent, String pattern, Object ... args)
	{
		info(indent, Strings.format(pattern, args));
	}

	/**
	 * Logs an indented {@link Level#INFO INFO} message of the specified {@link Level} with a {@link Throwable}.
	 * @param indent the number of space indent
	 * @param throwable the throwable to trace
	 * @param message the message to log
	 */
	public static void info(int indent, Throwable throwable, Object message)
	{
		LOGGER.info(indent(indent, message), throwable);
	}

	/**
	 * Logs an indented {@link Level#INFO INFO} message with pattern and {@link Throwable}.
	 * @param indent the number of space indent
	 * @param throwable the throwable to trace
	 * @param pattern the message pattern
	 * @param args the arguments to replace with
	 */
	public static void info(int indent, Throwable throwable, String pattern, Object ... args)
	{
		info(indent, throwable, Strings.format(pattern, args));
	}

	//--WARN

	/**
	 * Logs an {@link Level#WARN WARN} message.
	 * @param message
	 */
	public static void warn(String message)
	{
		LOGGER.warn(message);
	}

	/**
	 * Logs an {@link Level#WARN WARN} message with pattern.
	 * @param pattern the message pattern
	 * @param args the arguments to replace
	 */
	public static void warn(String pattern, Object ... args)
	{
		warn(Strings.format(pattern, args));
	}

	/**
	 * Logs an {@link Level#WARN WARN} message of the specified {@link Level} with a {@link Throwable}.
	 * @param throwable
	 * @param message
	 */
	public static void warn(Throwable throwable, String message)
	{
		LOGGER.warn(message, throwable);
	}

	/**
	 * Logs an {@link Level#WARN WARN} message with pattern and {@link Throwable}.
	 * @param throwable
	 * @param pattern
	 * @param args
	 */
	public static void warn(Throwable throwable, String pattern, Object ... args)
	{
		warn(throwable, Strings.format(pattern, args));
	}

	/**
	 * Logs an {@link Level#WARN WARN} message.
	 * @param message
	 */
	public static void warn(int indent, String message)
	{
		warn(Strings.repeat(' ', indent) + message);
	}

	/**
	 * Logs an {@link Level#WARN WARN} message with pattern.
	 * @param pattern the message pattern
	 * @param args the arguments to replace
	 */
	public static void warn(int indent, String pattern, Object ... args)
	{
		warn(indent, Strings.format(pattern, args));
	}

	/**
	 * Logs an {@link Level#WARN WARN} message of the specified {@link Level} with a {@link Throwable}.
	 * @param throwable
	 * @param message
	 */
	public static void warn(int indent, Throwable throwable, String message)
	{
		warn(throwable, Strings.repeat(' ', indent) + message);
	}

	/**
	 * Logs an {@link Level#WARN WARN} message with pattern and {@link Throwable}.
	 * @param throwable
	 * @param pattern
	 * @param args
	 */
	public static void warn(int indent, Throwable throwable, String pattern, Object ... args)
	{
		warn(indent, throwable, Strings.format(pattern, args));
	}


	//--ERROR

	/**
	 * Logs an {@link Level#ERROR ERROR} message.
	 * @param message
	 */
	public static void error(String message)
	{
		LOGGER.error(message);
	}

	/**
	 * Logs an {@link Level#ERROR ERROR} message with pattern.
	 * @param pattern the message pattern
	 * @param args the arguments to replace
	 */
	public static void error(String pattern, Object ... args)
	{
		error(Strings.format(pattern, args));
	}

	/**
	 * Logs an {@link Level#ERROR ERROR} message of the specified {@link Level} with a {@link Throwable}.
	 * @param throwable
	 * @param message
	 */
	public static void error(Throwable throwable, String message)
	{
		LOGGER.error(message, throwable);
	}

	/**
	 * Logs an {@link Level#ERROR ERROR} message with pattern and {@link Throwable}.
	 * @param throwable
	 * @param pattern
	 * @param args
	 */
	public static void error(Throwable throwable, String pattern, Object ... args)
	{
		error(throwable, Strings.format(pattern, args));
	}

	/**
	 * Logs an {@link Level#ERROR ERROR} message.
	 * @param message
	 */
	public static void error(int indent, String message)
	{
		error(Strings.repeat(' ', indent) + message);
	}

	/**
	 * Logs an {@link Level#ERROR ERROR} message with pattern.
	 * @param pattern the message pattern
	 * @param args the arguments to replace
	 */
	public static void error(int indent, String pattern, Object ... args)
	{
		error(indent, Strings.format(pattern, args));
	}

	/**
	 * Logs an {@link Level#ERROR ERROR} message of the specified {@link Level} with a {@link Throwable}.
	 * @param throwable
	 * @param message
	 */
	public static void error(int indent, Throwable throwable, String message)
	{
		error(throwable, Strings.repeat(' ', indent) + message);
	}

	/**
	 * Logs an {@link Level#ERROR ERROR} message with pattern and {@link Throwable}.
	 * @param throwable
	 * @param pattern
	 * @param args
	 */
	public static void error(int indent, Throwable throwable, String pattern, Object ... args)
	{
		error(indent, throwable, Strings.format(pattern, args));
	}


	//--DEBUG

	/**
	 * Logs an {@link Level#DEBUG DEBUG} message.
	 * @param message
	 */
	public static void debug(String message)
	{
		LOGGER.debug(message);
	}

	/**
	 * Logs an {@link Level#DEBUG DEBUG} message with pattern.
	 * @param pattern the message pattern
	 * @param args the arguments to replace
	 */
	public static void debug(String pattern, Object ... args)
	{
		debug(Strings.format(pattern, args));
	}

	/**
	 * Logs an {@link Level#DEBUG DEBUG} message of the specified {@link Level} with a {@link Throwable}.
	 * @param throwable
	 * @param message
	 */
	public static void debug(Throwable throwable, String message)
	{
		LOGGER.debug(message, throwable);
	}

	/**
	 * Logs an {@link Level#DEBUG DEBUG} message with pattern and {@link Throwable}.
	 * @param throwable
	 * @param pattern
	 * @param args
	 */
	public static void debug(Throwable throwable, String pattern, Object ... args)
	{
		debug(throwable, Strings.format(pattern, args));
	}

	/**
	 * Logs an {@link Level#DEBUG DEBUG} message.
	 * @param message
	 */
	public static void debug(int indent, String message)
	{
		debug(Strings.repeat(' ', indent) + message);
	}

	/**
	 * Logs an {@link Level#DEBUG DEBUG} message with pattern.
	 * @param pattern the message pattern
	 * @param args the arguments to replace
	 */
	public static void debug(int indent, String pattern, Object ... args)
	{
		debug(indent, Strings.format(pattern, args));
	}

	/**
	 * Logs an {@link Level#DEBUG DEBUG} message of the specified {@link Level} with a {@link Throwable}.
	 * @param throwable
	 * @param message
	 */
	public static void debug(int indent, Throwable throwable, String message)
	{
		debug(throwable, Strings.repeat(' ', indent) + message);
	}

	/**
	 * Logs an {@link Level#DEBUG DEBUG} message with pattern and {@link Throwable}.
	 * @param throwable
	 * @param pattern
	 * @param args
	 */
	public static void debug(int indent, Throwable throwable, String pattern, Object ... args)
	{
		debug(indent, throwable, Strings.format(pattern, args));
	}
}
