package utils;

/**
 * Provides conversions methods
 * @author m.renault
 */
public final class Conversions
{
	/** Converts the given object to its string representation
	 * @param value the value to convert
	 * @return the string representation or an empty string if value is null
	 */
	public static String toString(Object value)
	{
		return toString(value, "");
	}

	/** Converts the given object to its string representation
	 * @param value the value to convert
	 * @return the string representation or an empty string if value is null
	 */
	public static String toString(Object value, String defaultValue)
	{
		if(value == null)
		{
			return defaultValue;
		}
		else
		{
			try
			{
				return value.toString();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return defaultValue;
			}
		}
	}

	//--Integer

	/**
	 * Converts the given string to a integer
	 * @param value the value to convert
	 * @return the integer representation of the value or 0 if value is null or invalid
	 */
	public static int toInt(String value)
	{
		return toInt(value, 0);
	}

	/**
	 * Converts the given string to a integer
	 * @param value the value to convert
	 * @return the integer representation of the value or <code>defaultValue</code> if value is null or invalid
	 */
	public static int toInt(String value, int defaultValue)
	{
		if(value == null)
		{
			return defaultValue;
		}
		else
		{
			try
			{
				return Integer.valueOf(value);
			}
			catch(NumberFormatException e)
			{
				e.printStackTrace();
				return defaultValue;
			}
		}
	}
}
