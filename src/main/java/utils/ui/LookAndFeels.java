package utils.ui;

import java.util.HashMap;
import java.util.Map;

import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import utils.maths.MathUtils;

/**
 * Provides methods to manipulates system {@link LookAndFeel}
 * @author m.renault
 *
 */
public final class LookAndFeels
{
	//--Constants
	public static final LookAndFeelInfo METAL;
	public static final LookAndFeelInfo NIMBUS;
	public static final LookAndFeelInfo CDE_MOTIF;
	public static final LookAndFeelInfo WINDOWS;
	public static final LookAndFeelInfo WINDOWS_CLASSIC;

	static
	{
		Map<String, LookAndFeelInfo> available = new HashMap<>();

		//Get all available look and feel
		for(LookAndFeelInfo info : getAvailableLookAndFeel())
		{
			available.put(info.getName(), info);
		}

		//Assign
		METAL = available.getOrDefault("Metal", null);
		NIMBUS = available.getOrDefault("Nimbus", null);
		CDE_MOTIF = available.getOrDefault("CDE/Motif", null);
		WINDOWS = available.getOrDefault("Windows", null);
		WINDOWS_CLASSIC = available.getOrDefault("Windows Classic", null);
	}

	//--LookAndFeel

	/**
	 * Gets a random {@link LookAndFeelInfo}.
	 * @return a random {@link LookAndFeelInfo} from {@link #getAvailableLookAndFeel()}
	 */
	public static LookAndFeelInfo getRandomLookAndFeel()
	{
		return MathUtils.random(getAvailableLookAndFeel());
	}

	/**
	 * Gets all available {@link LookAndFeelInfo} on this system.
	 * @return an array of available {@link LookAndFeelInfo}
	 */
	public static LookAndFeelInfo[] getAvailableLookAndFeel()
	{
		return UIManager.getInstalledLookAndFeels();
	}

	/**
	 * Sets the lookAndFeel of this system from the given {@link LookAndFeelInfo}.
	 * @param lafInfo the {@link LookAndFeelInfo} to set
	 */
	public static void setLookAndFeel(LookAndFeelInfo lafInfo)
	{
		if(lafInfo != null)
		{
			setLookAndFeel(lafInfo.getClassName());
		}
		else
		{
			System.err.println("Unable to seet look and feel: null");
		}
	}

	/**
	 * Sets the lookAndFeel of this system from the given path.
	 * @param path the path of a {@link LookAndFeelInfo} class
	 */
	public static void setLookAndFeel(String path)
	{
		try
		{
			UIManager.setLookAndFeel(path);
		}
		catch(Throwable t)
		{
			System.err.println("Unable to seet look and feel: " + t.getMessage());
		}
	}
}
