package utils.time;

import java.util.concurrent.TimeUnit;

import com.google.common.base.Preconditions;

/**
 * A object that tick with specific interval
 * @author m.renault
 */
public final class Ticker implements Runnable
{
	//--Properties
	private long intervalNanos;
	private TimeUnit unit;

	//--Listener
	private ITickListener listener;

	//--Flag
	private boolean running = false;

	//--

	/**
	 * Creates a ticker with interval and {@link TimeUnit}
	 * @param interval the interval duration
	 * @param unit the unit of the interval duration
	 */
	public Ticker(long interval, TimeUnit unit)
	{
		this.setInterval(interval, unit);
	}

	/**
	 * Creates a ticker with interval, {@link TimeUnit} and a {@link ITickListener} destination
	 * @param interval the interval duration
	 * @param unit the unit of the interval duration
	 * @param listener the listener to receive ticks
	 */
	public Ticker(long interval, TimeUnit unit, ITickListener listener)
	{
		this.setInterval(interval, unit);
		this.setListener(listener);
	}

	/**
	 * Sets the tick interval in the given {@link TimeUnit}
	 * @param interval the interval duration
	 * @param unit the time unit of the interval
	 * @return
	 */
	public Ticker setInterval(long interval, TimeUnit unit)
	{
		Preconditions.checkState(interval > 0, "Tick interval cannot be inferior or equals than 0");
		Preconditions.checkState(unit != null, "Tick unit cannot be null");
		this.intervalNanos = unit.toNanos(interval);
		this.unit = unit;
		return this;
	}

	/** Starts the ticker. */
	public Ticker start()
	{
		Preconditions.checkState(!this.running, "Ticker already started");
		this.running = true;
		return this;
	}

	/** Stops the ticker. */
	public Ticker stop()
	{
		Preconditions.checkState(this.running, "Ticker not started");
		this.running = false;
		return this;
	}

	/**
	 * Sets the {@link ITickListener} for listening ticks
	 * @param listener the listener to receive ticks
	 */
	public Ticker setListener(ITickListener listener)
	{
		this.listener = listener;
		return this;
	}

	/***
	 * Gets the interval duration (in the appropriate {@link #getIntervalUnit() unit})
	 * @return the interval duration
	 */
	public long getInterval()
	{
		return this.getIntervalUnit().convert(this.intervalNanos, TimeUnit.NANOSECONDS);
	}

	/**
	 * Gets the {@link TimeUnit} of the interval duration
	 * @return the unit of the interval duration
	 */
	public TimeUnit getIntervalUnit()
	{
		return this.unit;
	}

	//--

	/**
	 * Runs the ticker.
	 * It locks the execution until {@link #stop()} is called. Must be used in another {@link Thread}
	 */
	@Override
	public void run()
	{
		long last = System.nanoTime();
		while(this.running)
		{
			long now = System.nanoTime();
			long elpased = now - last;

			if(elpased > this.intervalNanos)
			{
				//Tick
				if(this.listener != null)
				{
					this.listener.onTick();
				}

				last = now;
			}
		}
	}

	//--Interface

	/**
	 * A listener that receive {@link Ticker} ticks.
	 * @author m.renault
	 */
	public interface ITickListener
	{
		/** Called on {@link Ticker} tick. */
		public void onTick();
	}
}
