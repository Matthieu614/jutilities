package utils.time;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Preconditions;

/**
 * An object that can record elapsed time
 * @author m.renault
 */
public final class Timer
{
	//--Properties
	private boolean running = false;

	private long startNano = 0L;
	private long elapsedNano = 0L;

	private Timer() {}

	/** Starts the timer. */
	public Timer start()
	{
		Preconditions.checkState(!this.running, "Timer already running");
		this.startNano = System.nanoTime();
		this.running = true;
		return this;
	}

	/** Stops the timer. */
	public Timer stop()
	{
		Preconditions.checkState(this.running, "Timer is not running");
		this.running = false;
		this.elapsedNano = System.nanoTime() - this.startNano;
		return this;
	}

	/** Resets the timer. */
	public Timer reset()
	{
		this.elapsedNano = 0L;
		this.running = false;
		return this;
	}

	/** Indicates if this timer is running. */
	public boolean isRunning()
	{
		return this.running;
	}

	/** Gets the amount of elapsed nanoseconds since {@link #start() start}. */
	public long getElapsedNanos()
	{
		if(this.running)
		{
			return System.nanoTime() - this.startNano;
		}

		return this.elapsedNano;
	}

	/** Gets the elapsed time as a {@link Duration}. */
	public Duration getElapsed()
	{
		return Duration.ofNanos(this.getElapsedNanos());
	}

	@Override
	public String toString()
	{
		long elapsed = this.getElapsedNanos();

		TimeUnit unit = getAppropriateUnit(elapsed);
		double value = (double) elapsed / TimeUnit.NANOSECONDS.convert(1, unit);

		return String.format("%.4g", value) + " " + abbreviate(unit);
	}

	//--

	/** Gets the appropriate {@link TimeUnit} for the given nano time. */
	public static TimeUnit getAppropriateUnit(long nanos)
	{
		if(TimeUnit.DAYS.convert(nanos, TimeUnit.NANOSECONDS) > 0)
		{
			return TimeUnit.DAYS;
		}
		else if(TimeUnit.HOURS.convert(nanos, TimeUnit.NANOSECONDS) > 0)
		{
			return TimeUnit.HOURS;
		}
		else if(TimeUnit.MINUTES.convert(nanos, TimeUnit.NANOSECONDS) > 0)
		{
			return TimeUnit.MINUTES;
		}
		else if(TimeUnit.SECONDS.convert(nanos, TimeUnit.NANOSECONDS) > 0)
		{
			return TimeUnit.SECONDS;
		}
		else if(TimeUnit.MILLISECONDS.convert(nanos, TimeUnit.NANOSECONDS) > 0)
		{
			return TimeUnit.MILLISECONDS;
		}
		else if(TimeUnit.MICROSECONDS.convert(nanos, TimeUnit.NANOSECONDS) > 0)
		{
			return TimeUnit.MICROSECONDS;
		}
		else
		{
			return TimeUnit.NANOSECONDS;
		}
	}

	/**
	 * Gets the appropriate abbreviation for the given {@link TimeUnit}
	 * @param unit the unit to abbreviate
	 * @return the abbreviation of the unit
	 */
	public static String abbreviate(TimeUnit unit)
	{
		switch(unit)
		{
			case DAYS:
				return "d";
			case HOURS:
				return "h";
			case MINUTES:
				return "min";
			case SECONDS:
				return "s";
			case MILLISECONDS:
				return "ms";
			case MICROSECONDS:
				return "�s";
			case NANOSECONDS:
				return "ns";
		}

		throw new IllegalStateException();
	}

	//--

	/** Creates a {@link Timer}. */
	public static Timer createUnstarted()
	{
		return new Timer();
	}

	/** Creates a {@link Timer} and immediately start it. */
	public static Timer createStarted()
	{
		return createUnstarted().start();
	}
}
